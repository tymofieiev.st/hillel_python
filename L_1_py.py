import cmath

a = float(input('Enter a: '))
b = float(input('Enter b: '))
c = float(input('Enter c: '))


# calc the discriminant
d = (b ** 2) - (4 * a * c)


# find 2 solutions
sol1 = (-b - cmath.sqrt(d)) / (2 * a)
sol2 = (-b + cmath.sqrt(d)) / (2 * a)


print('The solution are {0} and {1}'.format(int(sol1.real), int(sol2.real)))
